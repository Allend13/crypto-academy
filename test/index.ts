import { expect } from 'chai'
import { ethers, network } from 'hardhat'
import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers'
import { BigNumber } from 'ethers'

import { $VotingContract } from '../typechain/$VotingContract'

type VotingInstance = [BigNumber, string, boolean, BigNumber] & {
  creationTime: BigNumber
  name: string
  isActive: boolean
  balance: BigNumber
}

describe('VotingContract', function () {
  let contract: $VotingContract
  let addr1ConnectedContract: $VotingContract
  let addr2ConnectedContract: $VotingContract
  let addr3ConnectedContract: $VotingContract

  let owner: SignerWithAddress
  let addr1: SignerWithAddress
  let addr2: SignerWithAddress
  let addr3: SignerWithAddress

  const CANDIDATE_1_NAME = 'Candidate 1'
  const CANDIDATE_2_NAME = 'Candidate 2'

  const VOTING_ID = 0
  const VOTING_NAME = 'My voting'

  const OWNER_COMMISSION_PERCENTS = 10
  const WEI_VOTE_PRICE = ethers.utils.parseEther('0.01')
  const WEI_VOTE_PRICE_MINUS = ethers.utils.parseEther('-0.01')

  beforeEach(async function () {
    const votingContractFactory = await ethers.getContractFactory(
      '$VotingContract'
    )
    const addresses = await ethers.getSigners()

    owner = addresses[0]
    addr1 = addresses[1]
    addr2 = addresses[2]
    addr3 = addresses[3]

    // @ts-ignore
    contract = await votingContractFactory.deploy()
    addr1ConnectedContract = await contract.connect(addr1)
    addr2ConnectedContract = await contract.connect(addr2)
    addr3ConnectedContract = await contract.connect(addr3)
  })

  /**
   * HELPER FUNCTIONS
   */
  async function createVoting() {
    const votingTxn = await contract.createVoting(VOTING_NAME, [
      {
        name: CANDIDATE_1_NAME,
        candidateAddress: addr1.address
      },
      {
        name: CANDIDATE_2_NAME,
        candidateAddress: addr2.address
      }
    ])

    await votingTxn.wait()
  }

  async function createAndGetVoting() {
    await createVoting()

    return contract.votingsList(VOTING_ID)
  }

  // Modify next block timestamp
  const DAYS_TIMEOUT_TIMESTAMP = 3 * 24 * 60 * 60
  async function blockTimestampDelay(
    voting: VotingInstance,
    timeoutModifier?: number
  ) {
    const [creationTime] = voting

    const timeout = Number(creationTime) + DAYS_TIMEOUT_TIMESTAMP
    await network.provider.send('evm_setNextBlockTimestamp', [
      timeoutModifier ? timeout + timeoutModifier : timeout
    ])
  }
  /**
   * HELPER FUNCTIONS END
   */

  describe('VotingContract.createVoting', function () {
    it('should create voting with a name and set it active', async function () {
      const [creationTime, name, isActive] = await createAndGetVoting()

      expect(creationTime).to.be.above(0)
      expect(name).equal(VOTING_NAME)
      expect(isActive).equal(true)
    })

    it('should add candidates to the voting', async function () {
      await createVoting()

      const candidate1Addr = await contract.votingCandidatesList(VOTING_ID, 0)
      const candidate2Addr = await contract.votingCandidatesList(VOTING_ID, 1)

      expect(candidate1Addr).equal(addr1.address)
      expect(candidate2Addr).equal(addr2.address)

      const candidate1Meta = await contract.votingCandidates(
        VOTING_ID,
        addr1.address
      )

      const candidate2Meta = await contract.votingCandidates(
        VOTING_ID,
        addr2.address
      )

      expect(candidate1Meta.name).equal(CANDIDATE_1_NAME)
      expect(candidate1Meta.votesCount).equal(0)
      expect(candidate1Meta.isStruct).to.be.true

      expect(candidate2Meta.name).equal(CANDIDATE_2_NAME)
      expect(candidate2Meta.votesCount).equal(0)
      expect(candidate2Meta.isStruct).to.be.true
    })

    it('should not let non-owner to create voting', async function () {
      const createVoting = () =>
        addr1ConnectedContract.createVoting(VOTING_NAME, [])

      await expect(createVoting()).to.be.reverted
    })
  })

  describe('VotingContract.closeVoting', function () {
    let votingCreated: VotingInstance

    beforeEach(async function () {
      votingCreated = await createAndGetVoting()
    })

    it('should close voting if 3 days passed since Votig creation', async function () {
      await blockTimestampDelay(votingCreated)

      await contract.closeVoting(VOTING_ID)
      const [, , isActive] = await contract.votingsList(VOTING_ID)

      expect(isActive).equal(false)
    })

    it('should not let close voting if 3 days not passed since Votig creation', async function () {
      await blockTimestampDelay(votingCreated, -100)

      const closeVoting = () => contract.closeVoting(VOTING_ID)

      await expect(closeVoting()).to.be.reverted
    })

    it('should let close voting for non-owner', async function () {
      await blockTimestampDelay(votingCreated)
      await addr1ConnectedContract.closeVoting(VOTING_ID)
      const [, , isActive] = await contract.votingsList(VOTING_ID)

      expect(isActive).equal(false)
    })

    it('should not let close voting more than once', async function () {
      await blockTimestampDelay(votingCreated)
      await contract.closeVoting(VOTING_ID)

      const closeVoting = () => contract.closeVoting(VOTING_ID)

      await expect(closeVoting()).to.be.reverted
    })

    it('should transfer all Voting balance to Winner exept 10% commission', async function () {
      // Addr1 would be the winner
      await contract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr1ConnectedContract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr3ConnectedContract.vote(VOTING_ID, addr2.address, {
        value: WEI_VOTE_PRICE
      })
      await blockTimestampDelay(votingCreated)

      const [, , , votingBalance] = await contract.votingsList(VOTING_ID)

      const expectedWinnerWithdraw = votingBalance.sub(
        votingBalance.div(OWNER_COMMISSION_PERCENTS)
      )
      const makeClose = () => contract.closeVoting(VOTING_ID)

      await expect(makeClose).to.changeEtherBalance(
        addr1,
        expectedWinnerWithdraw
      )
    })

    it('should leave 10% commission on Voting.balance', async function () {
      // Addr1 would be the winner
      await contract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr1ConnectedContract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr3ConnectedContract.vote(VOTING_ID, addr2.address, {
        value: WEI_VOTE_PRICE
      })
      await blockTimestampDelay(votingCreated)

      const [, , , votingBalance] = await contract.votingsList(VOTING_ID)
      const expectedComission = votingBalance.div(OWNER_COMMISSION_PERCENTS)
      await contract.closeVoting(VOTING_ID)
      const [, , , votingBalanceAfter] = await contract.votingsList(VOTING_ID)

      await expect(votingBalanceAfter).equal(expectedComission)
    })

    it('should not transfer anything, if no votes made', async function () {
      const balanceBefore = await contract.provider.getBalance(contract.address)

      await blockTimestampDelay(votingCreated)
      await contract.closeVoting(VOTING_ID)

      const balanceAfter = await contract.provider.getBalance(contract.address)
      const [, , , votingBalance] = await contract.votingsList(VOTING_ID)

      await expect(balanceBefore).equal(balanceAfter)
      await expect(votingBalance).equal(0)
    })
  })

  // TODO: add checking revert messages?
  describe('VotingContract.vote', function () {
    let votingCreated: VotingInstance

    beforeEach(async function () {
      votingCreated = await createAndGetVoting()
    })

    it('should add 1 vote to candidate in voting', async function () {
      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)
      await addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
        value: WEI_VOTE_PRICE
      })

      const [, , votesCount] = await contract.votingCandidates(
        VOTING_ID,
        candidateAddress
      )

      expect(votesCount).equal(1)
    })

    it('should not let signer vote twice', async function () {
      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)
      await addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
        value: WEI_VOTE_PRICE
      })

      const makeVote = () =>
        addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
          value: WEI_VOTE_PRICE
        })

      await expect(makeVote()).to.be.reverted
    })

    it('should not vote for сlosed voting', async function () {
      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)
      await blockTimestampDelay(votingCreated)
      await contract.closeVoting(VOTING_ID)

      const makeVote = () =>
        addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
          value: WEI_VOTE_PRICE
        })

      await expect(makeVote()).to.be.reverted
    })

    it('should not let vote if 3 days passed since Votig creation', async function () {
      await blockTimestampDelay(votingCreated)

      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)

      const makeVote = () =>
        addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
          value: WEI_VOTE_PRICE
        })

      await expect(makeVote()).to.be.reverted
    })

    it('should not vote for non-existing voting', async function () {
      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)

      const makeVote = () =>
        addr1ConnectedContract.vote(2, candidateAddress, {
          value: WEI_VOTE_PRICE
        })

      await expect(makeVote()).to.be.reverted
    })

    it('should not vote for non-existing candidate', async function () {
      const makeVote = () =>
        addr1ConnectedContract.vote(VOTING_ID, addr3.address, {
          value: WEI_VOTE_PRICE
        })

      await expect(makeVote()).to.be.reverted
    })

    it('should transfer 0.01 ETH from voter to contract balance', async function () {
      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)
      const makeVote = () =>
        addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
          value: WEI_VOTE_PRICE
        })

      await expect(makeVote).to.changeEtherBalances(
        [addr1, contract],
        [WEI_VOTE_PRICE_MINUS, WEI_VOTE_PRICE]
      )
    })

    it('should update Voting.balance with 0.01 ETH in WEI', async function () {
      const [, , , balance] = votingCreated

      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)
      await addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
        value: WEI_VOTE_PRICE
      })

      const [, , , balanceAfter] = await contract.votingsList(VOTING_ID)

      expect(balanceAfter).equal(balance.add(WEI_VOTE_PRICE))
    })

    it('should not let pay other amount than 0.01 ETH', async function () {
      const candidateAddress = await contract.votingCandidatesList(VOTING_ID, 0)

      const makeVote = () =>
        addr1ConnectedContract.vote(VOTING_ID, candidateAddress, {
          value: ethers.utils.parseEther('0.05')
        })

      await expect(makeVote()).to.be.reverted
    })
  })

  describe('VotingContract.getWinner', function () {
    beforeEach(async function () {
      // Create voting
      await createAndGetVoting()

      // Make vote for candidates
      // Addr1 would be the winner
      await contract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr1ConnectedContract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr2ConnectedContract.vote(VOTING_ID, addr2.address, {
        value: WEI_VOTE_PRICE
      })
    })

    it('should return correct winner', async function () {
      const [winnerAddress] = await contract.$getWinner(VOTING_ID)
      await expect(winnerAddress).equal(addr1.address)
    })
  })

  describe('VotingContract.withdrawComission', function () {
    let finishedVotingCreated: VotingInstance
    let expectedComission: BigNumber

    beforeEach(async function () {
      // Create voting
      finishedVotingCreated = await createAndGetVoting()

      // Make vote for candidates
      await contract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr1ConnectedContract.vote(VOTING_ID, addr1.address, {
        value: WEI_VOTE_PRICE
      })
      await addr2ConnectedContract.vote(VOTING_ID, addr2.address, {
        value: WEI_VOTE_PRICE
      })

      // Close voting
      await blockTimestampDelay(finishedVotingCreated)
      const [, , , votingBalance] = await contract.votingsList(VOTING_ID)
      expectedComission = votingBalance.div(OWNER_COMMISSION_PERCENTS)
      await contract.closeVoting(VOTING_ID)
    })

    it('should withdraw 10% commission to owner', async function () {
      const makeWithdraw = () => contract.withdrawComission(VOTING_ID)

      await expect(makeWithdraw).to.changeEtherBalance(owner, expectedComission)
    })

    it('should not let commission to non-owner', async function () {
      const makeWithdraw = () =>
        addr1ConnectedContract.withdrawComission(VOTING_ID)

      await expect(makeWithdraw()).to.be.reverted
    })

    it('should update Voting.balance to 0', async function () {
      await contract.withdrawComission(VOTING_ID)
      const [, , , balance] = await contract.votingsList(VOTING_ID)

      expect(balance).equal(0)
    })

    it('should not withdraw for active Voting', async function () {
      // Add active voting, so it's id would be 1
      const voting = await createAndGetVoting()
      const withdrawComission = () => contract.withdrawComission(1)

      expect(withdrawComission()).to.be.reverted
    })
  })

  describe('VotingContract.getAllVotings', function () {
    const VOTING_1_NAME = 'Voting 1'
    const VOTING_2_NAME = 'Voting 2'

    beforeEach(async function () {
      const votingTxn1 = await contract.createVoting(VOTING_1_NAME, [
        {
          name: CANDIDATE_1_NAME,
          candidateAddress: addr1.address
        }
      ])

      const votingTxn2 = await contract.createVoting(VOTING_2_NAME, [
        {
          name: CANDIDATE_2_NAME,
          candidateAddress: addr2.address
        }
      ])

      await votingTxn1.wait()
      await votingTxn2.wait()
    })

    it('should return all votings array', async function () {
      const allVotings = await contract.getAllVotings()

      expect(allVotings.length).equal(2)

      const [, name1] = allVotings[0]
      const [, name2] = allVotings[1]

      expect(name1).equal(VOTING_1_NAME)
      expect(name2).equal(VOTING_2_NAME)
    })
  })

  describe('VotingContract.getVotingCandidates', function () {
    beforeEach(async function () {
      await createVoting()
    })

    it('should return all candidates in voting', async function () {
      const candidates = await contract.getVotingCandidates(VOTING_ID)

      expect(candidates.length).equal(2)

      const [address1, name1] = candidates[0]
      const [address2, name2] = candidates[1]

      expect(address1).equal(addr1.address)
      expect(name1).equal(CANDIDATE_1_NAME)
      expect(address2).equal(addr2.address)
      expect(name2).equal(CANDIDATE_2_NAME)
    })
  })
})
