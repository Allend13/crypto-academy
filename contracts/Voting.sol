//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";


contract VotingContract {
    address payable public owner;
    uint internal ownerComissionPercents = 10;
    
    constructor() {
        owner = payable(msg.sender);
    }

    // Input data for creating Candidate
    struct CandidateInput {
        address candidateAddress;
        string name;
    }
    
    // Voting candidate
    struct CandidateMeta {
        address candidateAddress;
        string name;
        uint votesCount;
        // For checking of the existence of the struct
        bool isStruct;
    }

    // Voting
    struct Voting {
        uint creationTime;
        string name;
        bool isActive;
        uint balance;
    }

    // List of all voting
    Voting[] public votingsList;

    // Voting ID => Candidate addresses in Voting
    mapping(uint => address[]) public votingCandidatesList;
    // Voting ID => Candidate address => CandidateMeta
    mapping(uint => mapping(address => CandidateMeta)) public votingCandidates;

    // Votes made by Senders. 
    // Sender address => Votings IDs
    mapping(address => uint[]) public votesMade;

    // Restrcits to owner
    modifier ownerOnly() {
        require(msg.sender == owner, "Owner access required");
        _;
    }   

    // Check if Voting exists
    modifier ifVotingExists(uint _votingId) {
         if (_votingId >= votingsList.length) {
            revert("Voting doesn't exist");
        }

        _;
    }  

    function createVoting(string memory _name, CandidateInput[] memory _candidates) public ownerOnly {
        votingsList.push();
        uint id = votingsList.length - 1;
        
        votingsList[id].name = _name;
        votingsList[id].creationTime = block.timestamp;
        votingsList[id].isActive = true;
        
        // Add voting candidates
        for (uint256 i = 0; i < _candidates.length; i++) {
            address currentCandidateAddress = _candidates[i].candidateAddress;
            votingCandidatesList[id].push(_candidates[i].candidateAddress);
            votingCandidates[id][_candidates[i].candidateAddress] = CandidateMeta(currentCandidateAddress, _candidates[i].name, 0, true);
        }
    }

    function shouldVotingBeClosed(uint _votingId) internal view returns(bool) {
        return block.timestamp >= votingsList[_votingId].creationTime + 3 * 24 * 60 * 60;
    }

    function getWinner(uint _votingId) internal view returns(CandidateMeta memory winner) {
        // TODO: cases for several winners
        for (uint i = 0; i < votingCandidatesList[_votingId].length; i++) {
            address candidateAddress = votingCandidatesList[_votingId][i];
            CandidateMeta memory candidate = votingCandidates[_votingId][candidateAddress];

            if (candidate.votesCount > winner.votesCount) {
                winner = candidate;
            }
        }

        return winner;
    }

    function closeVoting(uint _votingId) public ifVotingExists(_votingId) {
        // Revert if Voting is already closed
         if (!votingsList[_votingId].isActive) {
            revert("Voting is already closed");
        }
    
        // Revert if no 3 days passed from Voting creation time
        if (!shouldVotingBeClosed(_votingId)) {
            revert("You can close Voting in 3 days");
        }

        // Close Voting 
        votingsList[_votingId].isActive = false;

        // Get winner
        CandidateMeta memory winner = getWinner(_votingId);

        // If winner has votes, than transfer his prize
        // If winner has not voice, than no votes are made at all
        if (winner.votesCount > 0) {
            uint prizeAmount = votingsList[_votingId].balance - (votingsList[_votingId].balance / ownerComissionPercents);

            // Leave 10% commission for Owner
            votingsList[_votingId].balance = votingsList[_votingId].balance - prizeAmount;
            payable(winner.candidateAddress).transfer(prizeAmount);
        }
    }

    function vote(uint _votingId, address _candidateAddress) public ifVotingExists(_votingId) payable {
        // Check if Voting is active
        if (!votingsList[_votingId].isActive) {
            revert("Voting has finished");
        }

        // Check if Voting should be closed
        if (votingsList[_votingId].isActive && shouldVotingBeClosed(_votingId)) {
            revert("Voting has finished. You can close it");
        }

        CandidateMeta memory candidate = votingCandidates[_votingId][_candidateAddress];
        // Check if Candidate exists inside voting
        if (!votingCandidates[_votingId][_candidateAddress].isStruct) {
            revert("Candidate doesn't exist");
        }

        // Check if sender has already voted
        for (uint256 i = 0; i < votesMade[msg.sender].length; i++) {
            if (votesMade[msg.sender][i] == _votingId) {
                revert("You've already voted");
            }
        }

        // Check if ETH amount is correct
        require(msg.value == 0.01 ether, "You need to send 0,01 ETH");
        
        // Upvote candidate
        uint newVotesCount = candidate.votesCount + 1;
        votingCandidates[_votingId][_candidateAddress].votesCount = newVotesCount;

        // Mark sender as already voted
        votesMade[msg.sender].push(_votingId);

        // Update voting balance
        // We do it last to be sure that nothing is reverted
        votingsList[_votingId].balance = votingsList[_votingId].balance + 0.01 ether;
    }

    function withdrawComission(uint _votingId) ownerOnly ifVotingExists(_votingId) public { 
        // Check if Voting is still active
        if (votingsList[_votingId].isActive) {
            revert("Voting still not finished");
        }

        owner.transfer(votingsList[_votingId].balance); 
        votingsList[_votingId].balance = 0;
    }

    function getAllVotings() public view returns(Voting[] memory) {
        Voting[] memory allVotings = new Voting[](votingsList.length);

        for (uint256 i = 0; i < votingsList.length; i++) {
            allVotings[i] = votingsList[i];
        }

        return allVotings;
    }

    function getVotingCandidates(uint _votingId) ifVotingExists(_votingId) public view returns(CandidateMeta[] memory) {
        address[] memory candidateAddresses = votingCandidatesList[_votingId];
        CandidateMeta[] memory allVotingCandidates = new CandidateMeta[](candidateAddresses.length);
        
        for (uint256 i = 0; i < candidateAddresses.length; i++) {
            address candidateAddress = candidateAddresses[i];
            allVotingCandidates[i] = votingCandidates[_votingId][candidateAddress];
        }

        return allVotingCandidates;
    }

}
